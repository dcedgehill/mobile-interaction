<?php
$root = "data/";
$zip_name = "data_set_2.zip";
$zip = new ZipArchive;
$zip->open($zip_name, ZipArchive::CREATE);
$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($root), RecursiveIteratorIterator::LEAVES_ONLY);
foreach ($files as $name => $file) {
    $new_filename = substr($name, strrpos($name, '/') + 1);
    $zip->addFile($file, $new_filename);
}
if (!$zip->close()) {
    echo 'There was a problem writing the ZIP archive.';
} else {
    echo 'Successfully created the ZIP Archive!';
    header("Content-type: application/zip");
    header('Content-Disposition: attachment; filename="' . $zip_name . '"');
    readfile($zip_name);
}