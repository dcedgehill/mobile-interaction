<?php
$files = glob('data/*');
foreach ($files as $file) {
    if (is_file($file)) {
        unlink($file);
    }
}
$pre_image = "";
for ($i = 0; $i < 11; $i++) {
    $folders_checked = array();
    include_once("connect_to_db.php");
    $sql = "SELECT * from record_mobile_interaction WHERE data_set = 2 AND test_data = 'false'";
    $exist_query = mysqli_query($mysqli, $sql);
    $exist = mysqli_num_rows($exist_query);
    while ($row = mysqli_fetch_array($exist_query)) {
        $device = $row["device"];
        $viewed = $row["viewed"];
        $held = $row["held"];
        $hand = $row["hand"];
        $width = 414;
        $height = 628;
        if ($viewed == "landscape") {
            $width = 736;
            $height = 370;
        }
        $dir = '../images/data/' . $device . '/' . $viewed . '/' . $held . '/' . $hand . '';
        if (!in_array($dir, $folders_checked)) {
            echo $dir;
            echo "<br>";
            array_push($folders_checked, $dir);
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if (strpos($file, '_' . $i . '_test.png') && !strpos($file, 'oq18') && !strpos($file, 'sbkf') && !strpos($file, '6ztc') && !strpos($file, '45f2') && !strpos($file, 'dvm6')) {
                            if (!$pre_image) {
                                $pre_image = $dir . '/' . $file;
                            } else {
                                echo $pre_image;
                                echo $dir . '/' . $file;
                                $image_1 = new Imagick($pre_image);
                                $image_2 = new Imagick($dir . '/' . $file);
                                $image_1->setSize($width, $height);
                                $image_2->setSize($width, $height);
                                if (!file_exists('data/' . $device . '_' . $viewed . '_' . $held . '_' . $hand . '_' . $i . '.png')) {
                                    echo "test";
                                    $image_1->evaluateImage(Imagick::EVALUATE_MULTIPLY, 0.43, Imagick::CHANNEL_ALPHA);

                                }
                                $image_2->evaluateImage(Imagick::EVALUATE_MULTIPLY, 0.43, Imagick::CHANNEL_ALPHA);
                                $image_1->compositeImage($image_2, Imagick::COMPOSITE_OVER, 0, 0);
                                $image_1->setImageFormat('png');
                                $image_1->writeImage('data/' . $device . '_' . $viewed . '_' . $held . '_' . $hand . '_' . $i . '.png');
                                $image_1->clear();
                                $image_2->clear();
                                /*$image_1 = imagecreatefrompng($pre_image);
                                $image_2 = imagecreatefrompng($dir . "/" . $file);
                                imagealphablending($image_1, true);
                                imagesavealpha($image_1, true);
                                imagealphablending($image_2, true);
                                imagesavealpha($image_2, true);
                                imagecolorallocatealpha($image_1, 0, 0, 0, 5);
                                imagecolorallocatealpha($image_2, 0, 0, 0, 5);
                                imagecopy($image_2, $image_1, 0, 0, 0, 0, $width, $height);
                                imagepng($image_2, 'data/' . $device . '_' . $viewed . "_" . $held . "_" . $hand . '_' . $i . '.png');
                                imagedestroy($image_1);
                                imagedestroy($image_2);*/
                                $pre_image = 'data/' . $device . '_' . $viewed . '_' . $held . '_' . $hand . '_' . $i . '.png';
                            }
                        }
                    }
                    closedir($dh);
                }
            }
            echo "Next line<br>";
            $pre_image = "";
        } else {
            echo "Already checked<br>";
        }
    }
    echo $i . " Test<br>";
}
echo "Successfully merged the PNG files into the relevant categories!";