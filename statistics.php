<?php
include_once("php/connect_to_db.php");
$held = "";
$viewed = "";
if (isset($_GET['held']) && $_GET['held'] == "smartphone_1" || $_GET['held'] == "smartphone_2" || $_GET['held'] == "smartphone_3" || $_GET['held'] == "smartphone_4" && $_GET['held'] == "tablet_1" || $_GET['held'] == "tablet_2" || $_GET['held'] == "tablet_3" || $_GET['held'] == "tablet_4" && $_GET['viewed'] && $_GET['viewed'] == "portrait" || $_GET['viewed'] == "landscape") {
    $held = preg_replace('#[^a-z0-9_]#i', '', $_GET['held']);
    $viewed = preg_replace('#[^a-z]#i', '', $_GET['viewed']);
    $messageOne = " in ";
    $messageTwo = " view - ";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $held; ?><?php echo $messageOne;?><?php echo $viewed; ?><?php echo $messageTwo;?>Statistics for mobile interaction application</title>
    <script src="app/app.js"></script>
    <script src="app/ajax.js"></script>
    <link href="app/app.css" rel="stylesheet"/>
</head>
<body>
<h1><?php echo $held; ?><?php echo $messageOne; ?><?php echo $viewed; ?><?php echo $messageTwo; ?>Statistics for mobile interaction application</h1>
<?php if ($held && $messageOne && $viewed && $messageTwo) : ?>
<canvas id="<?php echo $viewed; ?>"></canvas>
<?php else : ?>
<h2>Smartphones</h2>
<h3>Portrait</h3>
<div>
    <img class="selectStatistics" src="images/smartphone_1.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_1', 'portrait')">
    <img class="selectStatistics" src="images/smartphone_2.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_2', 'portrait')">
    <img class="selectStatistics" src="images/smartphone_3.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_3', 'portrait')">
    <img class="selectStatistics" src="images/smartphone_4.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_4', 'portrait')">
</div>
<h3>Landscape</h3>
<div>
    <img class="selectStatistics" src="images/smartphone_1.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_1', 'landscape')">
    <img class="selectStatistics" src="images/smartphone_2.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_2', 'landscape')">
    <img class="selectStatistics" src="images/smartphone_3.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_3', 'landscape')">
    <img class="selectStatistics" src="images/smartphone_4.fw.png" alt="Images must be enabled" onclick="openStatistics('smartphone_4', 'landscape')">
</div>
<h2>Tablets</h2>
<h3>Portrait</h3>
<div>
    <img class="selectStatistics" src="images/tablet_1.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_1', 'portrait')">
    <img class="selectStatistics" src="images/tablet_2.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_2', 'portrait')">
    <img class="selectStatistics" src="images/tablet_3.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_3', 'portrait')">
    <img class="selectStatistics" src="images/tablet_4.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_4', 'portrait')">
</div>
<h3>Landscape</h3>
<div>
    <img class="selectStatistics" src="images/tablet_1.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_1', 'landscape')">
    <img class="selectStatistics" src="images/tablet_2.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_2', 'landscape')">
    <img class="selectStatistics" src="images/tablet_3.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_3', 'landscape')">
    <img class="selectStatistics" src="images/tablet_4.fw.png" alt="Images must be enabled" onclick="openStatistics('tablet_4', 'landscape')">
</div>
<?php endif; ?>
</body>
</html>