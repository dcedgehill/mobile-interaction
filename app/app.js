var message, messageBox, interactWithMobile, defaultPrevent, btns = document.getElementsByTagName("button"), i, type, operatingSystem, browser, view, canvas, tempCanvas, canvasWidth, canvasHeight, timer, seconds = 5, addRecordTimer, addRecordSeconds = 0, deviceSeconds, tempStoreRecords = [], canvasMessage, handOne, handTwo, ctx, tempCtx, touchX, touchY, touchDown = 0, touchMoveStart = false, touches = 0, maxTouches = 0, ajax, choice1, choice2, choice3, orientationChange = false, statistics;

function id(x) { return document.getElementById(x); }

function loadApplication(step) {
    defaultPrevent = function(e) { e.preventDefault(); };
    document.body.parentElement.addEventListener("touchmove", defaultPrevent);
    document.body.addEventListener("touchmove", defaultPrevent);
    message = id("messageToUser");
    messageBox = id("messageBox");
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        type = mobileType();
        if (type != "unknown") {
            if (navigator.appVersion.indexOf("Win") != -1 || navigator.appVersion.indexOf("Mac") != -1 || navigator.appVersion.indexOf("X11") != -1 || navigator.appVersion.indexOf("Linux") != -1 || navigator.appVersion.indexOf("5.0 (Android 6.0.1)") != -1) {
                if (navigator.userAgent.search("MSIE") >= 0 || navigator.userAgent.search("Trident") >= 0 || navigator.userAgent.search("Edge") >= 0 || navigator.userAgent.search("Chrome") >= 0 || navigator.userAgent.search("Firefox") >= 0 || navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0 || navigator.userAgent.search("Opera") >= 0 || navigator.userAgent.search("Mobile/13D11") >= 0) {
                    if (step == 1) {
                        sessionStorage.clear();
                        message.innerHTML = "<h1>Interacting with a mobile device</h1><p>Previous research shows that website navigation can create issues, especially for users who navigate on mobile devices, such as smartphones or tablets. The purpose of this application is to analyse how people hold and interact with their mobile devices, as well as how far people can reach when holding and using a mobile device. This will determine what types of website navigation are usable (the difficulty of interacting with them) when interacting with them on non-mobile and mobile devices.</p><p>By using this application, you agree to partake in this research project and provide the required data, stated in the <a href='agreement.pdf' target='_blank'>PDF document (opens new window)</a>.</p><div class='centreBtn'><button onclick='loadApplication(2);'>Next</button></div>";

                    } else if (step == 2) {
                        message.innerHTML = "<h1>Select which hand do you use when interacting and using your device</h1><div class='centreBtn'><button onclick=\"saveChoice('left', 1)\">Left hand</button><button onclick=\"saveChoice('both', 1)\">Both hands</button><button onclick=\"saveChoice('right', 1)\">Right hand</button></div><div class=\"centreBtn\"><button onclick='loadApplication(1)'>Back</button></div>";
                    } else if (step == 3) {
                        sessionStorage.getItem(choice1);
                        if (type == "smartphone") {
                            if (sessionStorage.getItem("choice1") == "left") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img class='left' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"><img class='left' src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img class='left' src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else if (sessionStorage.getItem("choice1") == "right") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"><img src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else if (sessionStorage.getItem("choice1") == "both") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img src='images/" + type + "_4.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_4', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else {
                                loadApplication();
                            }
                        } else if (type == "tablet") {
                            if (sessionStorage.getItem("choice1") == "left") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img id='tablet' class='left' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else if (sessionStorage.getItem("choice1") == "right") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img id='tablet' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else if (sessionStorage.getItem("choice1") == "both") {
                                message.innerHTML = "<h1>Select how you hold your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view. If you hold your device similarly to any of the images, select the one that is the closest.</p><div id='choice'><img id='tablet' src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img id='tablet' src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"><img id='tablet' src='images/" + type + "_4.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_4', 2)\"></div><div class=\"centreBtn\"><button onclick='loadApplication(2)'>Back</button></div>";
                            } else {
                                loadApplication();
                            }
                        }
                    } else if (step == 4) {
                        if (sessionStorage.getItem("choice1") == "both") {
                            message.innerHTML = "<h1>Colour in the white canvas</h1><p>The following step requires you to colour in a white canvas when holding and using your mobile device. As soon as you select next, you will be introduced to the white canvas. Try to colour that white canvas using your " + handOne + " hand to do the first stroke, then use your " + handTwo + " hand to do the second stroke. Once you have done this, touch the OK button to proceed or touch the Cancel button to try again. After this, you will be able to confirm your choices for all of the steps before submitting them.</p><div class=\"centreBtn\"><button onclick='loadApplication(3)'>Back</button><button onclick='loadInteraction()'>Next</button></div>";
                        } else {
                            message.innerHTML = "<h1>Colour in the white canvas</h1><p>The following step requires you to colour in a white canvas when holding and using your mobile device. As soon as you select next, you will be introduced to the white canvas. Try to colour that white canvas using only one stroke. Once you have done this, touch the OK button to proceed or touch the Cancel button to try again. After this, you will be able to confirm your choices for all of the steps before submitting them.</p><div class=\"centreBtn\"><button onclick='loadApplication(3)'>Back</button><button onclick='loadInteraction()'>Next</button></div>";
                        }
                    } else if (step == 5) {
                        messageBox.style.opacity = 1;
                        message.innerHTML = "<h1>Confirm your choices</h1><p>If you are happy with your choices, touch the Submit button to send them off, along with the type of mobile device you are using and in the view you have had it in from when the application loaded. If you want to review your choices, touch the Back button.</p><p id='status'>Submitting data...</p><div class=\"centreBtn\"><button id='btn2' onclick='loadApplication(4)'>Back</button><button id='btn1' onclick='submitData()'>Submit</button></div>";
                        setTimeout(function() {
                            sessionStorage.setItem("dupInteraction", "true");
                        }, 256);
                    } else if (step == 6) {
                        if (view == "portrait") {
                            view = "landscape";
                        } else if (view == "landscape") {
                            view = "portrait";
                        }
                        if (type == "smartphone") {
                            type = "tablet";
                        } else if (type == "tablet") {
                            type = "smartphone";
                        }
                        message.innerHTML = "<h1>Your choices have been submitted</h1><p>Thank you for partaking in this application. Why not try the application by holding your device in " + view + " view and/or using a " + type + " if you haven't done so already.</p><div class='centreBtn'><button onclick='loadApplication(1)'>Retry</button></div>";
                        sessionStorage.clear();
                    } else {
                        sessionStorage.clear();
                        message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that the application has not detected your choices. Please reload the application and try again. If you think this is not the case, please contact the owner of this application.</p>";
                    }
                } else {
                    sessionStorage.clear();
                    message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that the browser you are using on your mobile device is not recognised by the application. Please use a different browser, different mobile device, or contact the owner of this application with the following information:</p><p>" + navigator.userAgent + "</p><p>As a result, you will be redirected back to start of the application.</p>";
                }
            } else {
                sessionStorage.clear();
                message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that the operating system on your mobile device is not recognised by the application. Please use a different mobile device or contact the owner of this application with the following information:</p><p>" + navigator.appVersion + "</p><p>As a result, you will be redirected back to start of the application.</p>";
            }
        } else {
            sessionStorage.clear();
            message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that we are unable to detect whether your device is a smartphone or tablet. Please visit this application on a mobile device (smartphone or tablet). If you think this is not the case, please contact the owner of this application.</p>";
        }
    } else {
        sessionStorage.clear();
        message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that you are on a desktop device. Please visit this application on a mobile device (smartphone or tablet). If you think this is not the case, please contact the owner of this application.</p>";
    }
    for (i = 0; i < btns.length; i++) {
        btns[i].disabled = true;
    }
    setTimeout(function() {
        message.style.visibility = "visible";
        message.style.opacity = 1;
    }, 256);
    for (i = 0; i < btns.length; i++) {
        btns[i].disabled = false;
    }
}

function detectOrientationChange() {
    if (window.orientation == 90 || window.orientation == -90) {
        view = "landscape";
        handOne = "left";
        handTwo = "right";
        //positionTrace = -50;
        /*canvasWidth = screen.height;
         canvasHeight = screen.width;*/
        /*canvasWidth = window.innerHeight;
         canvasHeight = window.innerWidth;*/
        /*if (type == "smartphone") {
         canvasWidth = 736;
         canvasHeight = 414;
         } else if (type == "tablet") {
         canvasWidth = 1024;
         canvasHeight = 768;
         }*/
    } else {
        view = "portrait";
        handOne = "right";
        handTwo = "left";
        //positionTrace = 25;
        /*canvasWidth = screen.width;
         canvasHeight = screen.height;*/
        /*canvasWidth = window.innerWidth;
         canvasHeight = window.innerHeight;*/
        /*if (type == "smartphone") {
         canvasWidth = 414;
         canvasHeight = 736;
         } else if (type == "tablet") {
         canvasWidth = 768;
         canvasHeight = 1024;
         }*/
    }
    if (orientationChange == true) {
        alert("Oh dear. It seems that the orientation on your mobile device has changed. Please stick with either portrait or landscape and not swap while the application is running. If you think this is not the case, please contact the owner of this application.\n\nAs a result, you will be redirected back to start of the application.");
        location.reload();
    }
    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;
    orientationChange = true;
}

function drawDot(ctx, x, y, size) {
    r =0;
    g = 0;
    b = 255;
    a = 255;
    ctx.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + (a / 255) + ")";
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
}

function clearCanvas(canvas,ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function touchStart() {
    clearInterval(timer);
    canvasMessage = id("canvasMessage");
    canvasMessage.innerHTML = "";
    touchDown = 1;
    drawDot(ctx, touchX, touchY, 40);
}

function touchEnd() {
    if (!sessionStorage["dupInteraction"]) {
        return;
    }
    sessionStorage.removeItem("dupInteraction");
    clearInterval(timer);
    clearInterval(addRecordTimer);
    interactWithMobile = id("interactWithMobile");
    if (interactWithMobile.style.display == "block") {
        touchDown = 0;
        touchMoveStart = false;
        touches++;
        if (sessionStorage.getItem("choice1") == "both" && maxTouches != 1) {
            sessionStorage.setItem("dupInteraction", "false");
            maxTouches = 1;
            addRecordSeconds = 0;
        }
        if (touches > maxTouches) {
            var r = confirm("If you are happy with the result, press OK. Otherwise, press Cancel to try again.");
            if (r == true) {
                if (type == "tablet") {
                    tempCtx.drawImage(canvas, 0, 0, tempCanvas.width, tempCanvas.height);
                } else {
                    tempCanvas = canvas;
                }
                tempStoreRecords.push(tempCanvas.toDataURL("image/png"));
                saveDrawing(tempStoreRecords);
            } else {
                sessionStorage.setItem("dupInteraction", "false");
                /*if (sessionStorage.getItem("choice1") == "both") {
                 seconds = 10;
                 } else {
                 seconds = 5;
                 }*/
                //timer = setInterval(secondsRemaining, 1000);
            }
            touches = 0;
            maxTouches = 0;
            addRecordSeconds = 0;
            tempStoreRecords = [];
            clearCanvas(canvas, ctx);
            clearCanvas(tempCanvas, tempCtx);
            /*if (sessionStorage.getItem("choice1") == "both") {
             interactWithMobile.classList.remove("flipTrace");
             canvas.classList.remove("flipContent");
             }*/

            /*} else {
             interactWithMobile.classList.add("flipTrace");
             canvas.classList.add("flipContent");*/
        }
    }
}

function touchMove(e) {
    getTouchPos(e);
    if (touchDown == 1) {
        drawDot(ctx, touchX, touchY, 40);
        if (touchMoveStart != true) {
            touchMoveStart = true;
            addRecordTimer = setInterval(addRecording, deviceSeconds);
        }
    }
}

function getTouchPos(e) {
    if (!e) {
        e = event;
    }
    if (e.offsetX) {
        touchX = e.offsetX;
        touchY = e.offsetY;
    } else if (e.layerX) {
        touchX = e.layerX;
        touchY = e.layerY;
    }
}

function loadDrawing() {
    canvas = id("draw");
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    if (canvas.getContext) {
        ctx = canvas.getContext('2d');
    }
    if (ctx) {
        canvas.addEventListener("touchstart", touchStart, false);
        canvas.addEventListener("touchmove", touchMove, false);
        window.addEventListener("touchend", touchEnd, false);
    }
    if (type == "tablet") {
        tempCanvas = id("tempDraw");
        if (view == "portrait") {
            tempCanvas.width = 414;
            tempCanvas.height = 628;
        } else if (view == "landscape") {
            tempCanvas.width = 736;
            tempCanvas.height = 370;
        }
        if (tempCanvas.getContext) {
            tempCtx = tempCanvas.getContext('2d');
        }
    }
}

function showInfo() {
    seconds--;
    if (seconds <= 0) {
        canvasMessage = id("canvasMessage");
        if (sessionStorage.getItem("choice1") == "both") {
            canvasMessage.innerHTML = "Colour this white canvas using your " + handOne + " hand to do the first stroke, then use your " + handTwo + " hand to do the second stroke.";
        } else {
            canvasMessage.innerHTML = "Colour this white canvas using only one stroke.";
        }
    }
}

/*function secondsRemaining() {
 seconds--;
 if (seconds <= 0) {
 touches++;
 touchEnd();
 }
 }*/

function addRecording() {
    addRecordSeconds++;
    if (addRecordSeconds > 5) {
        clearInterval(addRecordTimer);
        return;
    }
    if (type == "tablet") {
        tempCtx.drawImage(canvas, 0, 0, tempCanvas.width, tempCanvas.height);
    } else {
        tempCanvas = canvas;
    }
    tempStoreRecords.push(tempCanvas.toDataURL("image/png"));
}

function loadInteraction() {
    for (i = 0; i < btns.length; i++) {
        btns[i].disabled = true;
    }
    messageBox = id("messageBox");
    messageBox.style.opacity = 0;
    interactWithMobile = id("interactWithMobile");
    //interactWithMobile.onclick = function() { saveDrawing(event) };
    interactWithMobile.style.display = "block";
    interactWithMobile.style.zIndex = 2;
    setTimeout(function() {
        sessionStorage.setItem("dupInteraction", "false");
        timer = setInterval(showInfo, 1000);
    }, 1024);
    /*if (sessionStorage.getItem("choice1") == "both") {
     seconds = 10;
     } else {
     seconds = 5;
     }
     setTimeout(function() {
     timer = setInterval(secondsRemaining, 1000);
     }, 256);*/
    /*setTimeout(function() {
     interactWithMobile.classList.remove("flipTrace");
     canvas.classList.remove("flipContent");
     if (sessionStorage.getItem("choice1") == "left") {
     interactWithMobile.classList.add("flipTrace");
     canvas.classList.add("flipContent");
     }
     interactWithMobile.style.backgroundImage = "url('images/" + type + "_" + view + "_trace.fw.png')";
     interactWithMobile.style.backgroundRepeat = "no-repeat";
     interactWithMobile.style.backgroundPosition = "right " + positionTrace + "px top";
     }, 1000);*/
}

function hideInteraction() {
    interactWithMobile = id("interactWithMobile");
    /*interactWithMobile.style.backgroundImage = "";
     interactWithMobile.style.backgroundRepeat = "";
     interactWithMobile.style.backgroundPosition = "";*/
    interactWithMobile.style.zIndex = -1;
    interactWithMobile.style.display = "none";
    //interactWithMobile.onclick = function() { return false };
}

function saveDrawing(canvasData) {
    hideInteraction();
    saveChoice(canvasData, 3)
}

function mobileType() {
    if (window.innerHeight > window.innerWidth) {
        if (window.innerWidth > 480) {
            //view = "portrait";
            deviceSeconds = 1200;
            return "tablet";
        }
        if (window.innerWidth <= 480) {
            //view = "portrait";
            deviceSeconds = 400;
            return "smartphone";
        }
    } else {
        if (window.innerWidth > 736) {
            //view = "landscape";
            deviceSeconds = 1200;
            return "tablet";
        }
        if (window.innerWidth <= 736) {
            //view = "landscape";
            deviceSeconds = 400;
            return "smartphone";
        }
    }
    return "unknown";
}

function mobileOS() {
    if (navigator.appVersion.indexOf("Win") != -1) {
        return "windows";
    } else if (navigator.appVersion.indexOf("Mac") != -1) {
        return "ios";
    } else if (navigator.appVersion.indexOf("X11") != -1) {
        return "unix";
    } else if (navigator.appVersion.indexOf("Linux") != -1 || navigator.appVersion.indexOf("5.0 (Android 6.0.1)") != -1) {
        return "linux";
    }
    return "unknown";
}

function mobileBrowser() {
    if (navigator.userAgent.search("MSIE") >= 0 || navigator.userAgent.search("Trident") >= 0 || navigator.userAgent.search("Edge") >= 0) {
        return "internet explorer";
    } else if (navigator.userAgent.search("Chrome") >= 0) {
        return "google chrome";
    } else if (navigator.userAgent.search("Firefox") >= 0) {
        return "mozilla firefox";
    } else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        return "apple safari";
    } else if (navigator.userAgent.search("Opera") >= 0) {
        return "opera";
    }
    return "unknown";
}

function saveChoice(choice, number) {
    sessionStorage.removeItem("choice" + number);
    if (number == 3) {
        sessionStorage.setItem("choice" + number, JSON.stringify(choice))
    } else {
        sessionStorage.setItem("choice" + number, choice);
    }
    loadApplication(number + 2);
}

function submitData() {
    if (sessionStorage.getItem("dupInteraction") == "false") {
        return;
    }
    sessionStorage.removeItem("dupInteraction");
    if (!sessionStorage["choice1", "choice2", "choice3"]) {
        loadApplication();
        return;
    }
    operatingSystem = mobileOS();
    if (operatingSystem == "unknown") {
        window.prompt("Oh dear. It seems that the operating system on your mobile device is not recognised by the application. Please use a different mobile device or contact the owner of this application with the information in the text field.\n\nAs a result, you will be redirected back to start of the application.", navigator.appVersion);
        location.reload();
        return;
    }
    browser = mobileBrowser();
    if (browser == "unknown") {
        window.prompt("Oh dear. It seems that the browser you are using on your mobile device is not recognised by the application. Please use a different browser, different mobile device, or contact the owner of this application with the information in the text field.\n\nAs a result, you will be redirected back to start of the application.", navigator.userAgent);
        location.reload();
        return;
    }
    id("btn1").disabled = true;
    id("btn2").disabled = true;
    id("status").style.display = "block";
    choice1 = sessionStorage.getItem("choice1");
    choice2 = sessionStorage.getItem("choice2");
    choice3 = sessionStorage.getItem("choice3");
    ajax = ajaxObj("POST", "index.php");
    ajax.onreadystatechange = function () {
        if (ajaxReturn(ajax) == true) {
            if (ajax.responseText == "submitted") {
                loadApplication(6);
            } else {
                alert(ajax.responseText);
                loadApplication();
            }
            id("status").style.display = "none";
            id("btn1").disabled = false;
            id("btn2").disabled = false;
        }
    };
    ajax.send("touched=" + choice3 + "&device=" + type + "&operatingSystem=" + operatingSystem + "&browser=" + browser + "&viewed=" + view + "&held=" + choice2 + "&hand=" + choice1 + "&width=" + canvasWidth + "&height=" + canvasHeight);
}

function openStatistics(held, viewed) {
    statistics = window.open("statistics.php?held=" + held + "&viewed=" + viewed, "", "width=1500, height=1000");
}