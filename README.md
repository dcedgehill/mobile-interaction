# Mobile Interaction Website Application README

Shows the process of how the Mobile Interaction Website Application was developed.

* Click on Source to see the code for the Mobile Interaction Website Application.
* To see the participant data, click on Source and check out the images/data, php/ and php/data folders for any PNG, SQL and Zip files.
* Visit https://www.mytimeworld.co.uk/apps/mobile_interaction/ on a mobile device to try out the website application.