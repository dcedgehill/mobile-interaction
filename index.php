<?php
if (isset($_POST["touched"])) {
    //$touched = preg_replace('#[^0-9,]#i', '', $_POST['touched']);
    //$touched = str_replace(',', ', ', $touched);
    $touched = $_POST['touched'];
    $touched = str_replace(' ', '+', $touched);
    $touched = str_replace('[', '', $touched);
    $touched = str_replace(']', '', $touched);
    $device = preg_replace('#[^a-z]#i', '', $_POST['device']);
    $operating_system = preg_replace('#[^a-z]#i', '', $_POST['operatingSystem']);
    $browser = preg_replace('#[^a-z ]#i', '', $_POST['browser']);
    $viewed = preg_replace('#[^a-z]#i', '', $_POST['viewed']);
    $held = preg_replace('#[^a-z0-9_]#i', '', $_POST['held']);
    $hand = preg_replace('#[^a-z]#i', '', $_POST['hand']);
    $width = preg_replace('#[^0-9]#i', '', $_POST['width']);
    $height = preg_replace('#[^0-9]#i', '', $_POST['height']);
    $touched = explode("\",\"", $touched);
    if ($touched == "" || $device == "" || $operating_system == "" || $browser == "" || $viewed == "" || $held == "" || $hand == "" || $width == "" || $height == ""){
        echo "failed 1";
        exit();
    } else if ($device != "smartphone" && $device != "tablet") {
        echo "failed 3";
        exit();
    } else if ($viewed != "portrait" && $viewed != "landscape") {
        echo "failed 4";
        exit();
    } else if ($hand != "left" && $hand != "both" && $hand != "right") {
        echo $hand;
        echo "failed 5";
        exit();
    } else if ($held != "smartphone_1" && $held != "smartphone_2" && $held != "smartphone_3" && $held != "smartphone_4" && $held != "tablet_1" && $held != "tablet_2" && $held != "tablet_3" && $held != "tablet_4") {
        echo "failed 6";
        exit();
    } else {
        include_once("php/connect_to_db.php");
        $sql = "INSERT INTO record_mobile_interaction(touched, device, operating_system, browser, viewed, held, hand, width, height) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("sssssssii", implode("\",\"", $touched), $device, $operating_system, $browser, $viewed, $held, $hand, $width, $height);
        $stmt->execute();
        $last_id = $mysqli->insert_id;
        $sql = "SELECT * from record_mobile_interaction WHERE id = $last_id limit 1";
        $exist_query = mysqli_query($mysqli, $sql);
        while ($row = mysqli_fetch_array($exist_query)) {
            $existing_id = $row["id"];
            $tester_id = $row["tester_id"];
        }
        // $tester_id = Per participant
        // $width = Width of user's device
        // $height = Height of user's device
        // $existing_id = Submissions that belong to each participant
        for ($i = 0; $i < count($touched); $i++) {
            $filename = 'images/data/' . $device . '/' . $viewed .'/' . $held . '/' . $hand . '/record_' . $tester_id . '_' . $operating_system . '_' . $browser . '_' . $width . '_' . $height . '_' . $existing_id . '_' . $i . '_test.png';
            $touched[$i] = str_replace("\"", '', $touched[$i]);
            $filteredTouched = substr($touched[$i], strpos($touched[$i], ",") + 1);
            $decodeTouched = base64_decode($filteredTouched);
            $file = fopen($filename, 'wb');
            fwrite($file, $decodeTouched);
            fclose($file);
        }
        $stmt->close();
        $mysqli->close();
        echo "submitted";
        exit();
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Interacting with your mobile device</title>
    <script src="app/app.js"></script>
    <script src="app/ajax.js"></script>
    <link href="app/app.css" rel="stylesheet"/>
</head>
<body>
<!-- Version 1.2.2 -->
<div id="messageBox">
    <div id="messageToUser"></div>
    <noscript>
        <div id="noScript">
            <h1>Error!</h1>
            <p>Oh dear. It seems that you do not have JavaScript enabled. Please enable it and reload the page. If you think this is not the case, please contact the owner of this application.</p>
        </div>
    </noscript>
</div>
<div id="interactWithMobile"><span id="canvasMessage"></span><canvas id="draw"></canvas><canvas id="tempDraw"></canvas></div>
<script>
    window.onload = loadApplication(1);
    window.addEventListener('orientationchange', detectOrientationChange);
    detectOrientationChange();
    window.addEventListener('load', function(event) {
        loadDrawing();
    });
</script>
</body>
</html>